package br.com.fruno.kotlinmessenger.ui.messages

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import br.com.fruno.kotlinmessenger.R
import br.com.fruno.kotlinmessenger.model.User
import br.com.fruno.kotlinmessenger.ui.messages.UserAdapter.UserViewHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.users_item.view.*

class UserAdapter(
    private val users: List<User>,
    private val context: Context,
    private val listener: OnUserClickListener
) :
    Adapter<UserViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.users_item, parent, false)
        return UserViewHolder(view, listener)
    }

    override fun getItemCount(): Int = users.size

    override fun onBindViewHolder(holderUser: UserViewHolder, position: Int) = holderUser.bind(users[position])

    class UserViewHolder(itemView: View, private val listener: OnUserClickListener) : ViewHolder(itemView) {

        fun bind(user: User) {
            itemView.setOnClickListener { listener.onUserClick(user) }
            itemView.users_item_name.text = user.username

            val circularProgressDrawable = CircularProgressDrawable(itemView.context)
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.centerRadius = 30f

            Glide.with(itemView)
                .load(user.photoUrl)
                .apply(RequestOptions().placeholder(circularProgressDrawable))
                .into(itemView.users_item_photo)
        }
    }

    interface OnUserClickListener {
        fun onUserClick(user: User)
    }
}