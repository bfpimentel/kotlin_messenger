package br.com.fruno.kotlinmessenger.ui.login

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import br.com.fruno.kotlinmessenger.R
import br.com.fruno.kotlinmessenger.model.User
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_register.*
import java.util.*

class RegisterActivity : AppCompatActivity() {

    companion object {
        const val TAG = "RegisterActivity"
        const val REQUEST_CODE_GALLERY = 0
        const val TYPE_IMAGE = "image/*"
    }

    private var selectedPhotoUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        supportActionBar?.title = "Register"
        register_bt_add_photo.setOnClickListener { addPhoto() }
        register_bt_register.setOnClickListener { createFirebaseUser() }
        register_tv_already_have_account.setOnClickListener { startActivity(Intent(this, LoginActivity::class.java)) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK) {
            return
        }

        if (requestCode == REQUEST_CODE_GALLERY && data != null) {
            selectedPhotoUri = data.data
            Glide.with(this).load(selectedPhotoUri).into(register_iv_photo)
            register_bt_add_photo.alpha = 0f
        }
    }

    private fun addPhoto() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = TYPE_IMAGE
        startActivityForResult(
            intent,
            REQUEST_CODE_GALLERY
        )
    }

    private fun createFirebaseUser() {
        val email = register_et_email.text.toString()
        val password = register_et_password.text.toString()

        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Fill email and password fields", Toast.LENGTH_LONG).show()
            return
        }

        FirebaseAuth.getInstance()
            .createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (!it.isSuccessful) return@addOnCompleteListener
                uploadImageToFirebaseStorage()
            }
            .addOnFailureListener {
                Log.d(TAG, "Error: ${it.message}")
            }
    }

    private fun uploadImageToFirebaseStorage() {
        if (selectedPhotoUri == null) return

        val filename = UUID.randomUUID().toString()
        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")
        ref.putFile(selectedPhotoUri!!)
            .addOnSuccessListener {
                ref.downloadUrl.addOnSuccessListener {
                    saveUserToFirebaseDatabase(it.toString())
                }.addOnFailureListener {
                    Log.d(TAG, "Error: ${it.message}")
                }
            }.addOnFailureListener {
                Log.d(TAG, "Error: ${it.message}")
            }
    }

    private fun saveUserToFirebaseDatabase(photoUrl: String) {
        val uid = FirebaseAuth.getInstance().uid ?: ""
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")
        val user = User(uid, register_et_username.text.toString(), photoUrl)

        ref.setValue(user)
            .addOnSuccessListener {
                startActivity(Intent(this, LoginActivity::class.java))
            }.addOnFailureListener {
                Log.d(TAG, "Error: ${it.message}")
            }
    }
}
