package br.com.fruno.kotlinmessenger.model

/** @author Bruno Pimentel on 19, January, 2019 **/
class Message(val id: String, val message: String, val fromId: String, val toId: String, val timeStamp: Long) {
    constructor() : this("", "", "", "", -1L)
}