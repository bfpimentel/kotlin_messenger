package br.com.fruno.kotlinmessenger.ui.messages

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.fruno.kotlinmessenger.R
import br.com.fruno.kotlinmessenger.model.User
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_new_message.*

class NewMessageActivity : AppCompatActivity(), UserAdapter.OnUserClickListener {

    companion object {
        var USER_ID = "USER_ID"
        var USER_NAME = "USER_NAME"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_message)
        supportActionBar?.title = "New Message"
        fetchUsers()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        onBackPressed()
        return true
    }

    override fun onUserClick(user: User) {
        val intent = Intent(this, ChatActivity::class.java)
        intent.putExtra(USER_ID, user.uid)
        intent.putExtra(USER_NAME, user.username)
        startActivity(intent)
        finish()
    }

    private fun fetchUsers() {
        val ref = FirebaseDatabase.getInstance().getReference("/users")
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val list: ArrayList<User> = ArrayList()
                dataSnapshot.children.forEach {
                    val user = it.getValue(User::class.java)
                    if (user != null) {
                        list.add(user)
                    }
                }
                setupRecyclerView(list)
            }

            override fun onCancelled(p0: DatabaseError) {
                //unused
            }
        })
    }

    private fun setupRecyclerView(users: List<User>) {
        val recyclerView = activity_new_message_rv_users
        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.adapter = UserAdapter(users, this, this)
    }
}
