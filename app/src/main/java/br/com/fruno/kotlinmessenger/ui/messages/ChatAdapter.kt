package br.com.fruno.kotlinmessenger.ui.messages

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import br.com.fruno.kotlinmessenger.R
import br.com.fruno.kotlinmessenger.model.Message
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.chat_sent_message.view.*

class ChatAdapter(private val messages: List<Message>, private val context: Context) :
    Adapter<ViewHolder>() {

    companion object {
        var MESSAGE_SENT = 0
        var MESSAGE_RECEIVED = 1
    }

    override fun getItemViewType(position: Int): Int {
        return if (messages[position].fromId == FirebaseAuth.getInstance().uid) MESSAGE_SENT
        else MESSAGE_RECEIVED
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if (viewType == MESSAGE_RECEIVED) {
            val view = LayoutInflater.from(context).inflate(R.layout.chat_received_message, parent, false)
            ReceivedMessageViewHolder(view)
        } else {
            val view = LayoutInflater.from(context).inflate(R.layout.chat_sent_message, parent, false)
            SentMessageViewHolder(view)
        }
    }

    override fun getItemCount(): Int = messages.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (holder.itemViewType) {
            MESSAGE_SENT -> (holder as SentMessageViewHolder).bind(messages[position])
            MESSAGE_RECEIVED -> (holder as ReceivedMessageViewHolder).bind(messages[position])
        }
    }

    class SentMessageViewHolder(itemView: View) : ViewHolder(itemView) {

        fun bind(message: Message) {
            itemView.chat_tv_message.text = message.message
        }
    }

    class ReceivedMessageViewHolder(itemView: View) : ViewHolder(itemView) {

        fun bind(message: Message) {
            itemView.chat_tv_message.text = message.message
        }
    }
}