package br.com.fruno.kotlinmessenger.model

class User(val uid: String, val username: String, val photoUrl: String) {
    constructor() : this("", "", "")
}