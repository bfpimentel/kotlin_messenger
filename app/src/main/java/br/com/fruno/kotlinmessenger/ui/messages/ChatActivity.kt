package br.com.fruno.kotlinmessenger.ui.messages

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.fruno.kotlinmessenger.R
import br.com.fruno.kotlinmessenger.model.Message
import br.com.fruno.kotlinmessenger.ui.messages.NewMessageActivity.Companion.USER_ID
import br.com.fruno.kotlinmessenger.ui.messages.NewMessageActivity.Companion.USER_NAME
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_chat.*

class ChatActivity : AppCompatActivity() {

    private val messages: ArrayList<Message> = ArrayList()
    private var adapter = ChatAdapter(messages, this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        supportActionBar?.title = intent.getStringExtra(USER_NAME)
        messageListener()
        initializeRecyclerView()
        chat_iv_send.setOnClickListener { sendMessage() }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        onBackPressed()
        return true
    }

    private fun initializeRecyclerView() {
        val recyclerView = chat_rv_messages
        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.adapter = adapter
    }

    private fun messageListener() {
        val fromId = FirebaseAuth.getInstance().uid
        val toId = intent.getStringExtra(USER_ID)

        val ref = FirebaseDatabase.getInstance().getReference("/messages/$fromId/$toId")

        ref.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(dataSnapshot: DataSnapshot, p1: String?) {
                val message = dataSnapshot.getValue(Message::class.java)
                if (message != null) {
                    messages.add(message)
                    adapter.notifyDataSetChanged()
                }
            }

            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
            }

            override fun onChildRemoved(p0: DataSnapshot) {
            }
        })
    }

    private fun sendMessage() {
        val message = chat_et_message.text.toString()

        if (message.isEmpty()) return

        val fromId = FirebaseAuth.getInstance().uid
        val toId = intent.getStringExtra(USER_ID)
        val fromRef = FirebaseDatabase.getInstance().getReference("/messages/$fromId/$toId").push()
        val toRef = FirebaseDatabase.getInstance().getReference("/messages/$toId/$fromId").push()

        if (fromId == null) return

        val chatMessage = Message(fromRef.key!!, message, fromId, toId, System.currentTimeMillis())
        fromRef.setValue(chatMessage)
            .addOnSuccessListener {
                chat_et_message.text?.clear()
                chat_rv_messages.scrollToPosition(messages.size)
            }

        toRef.setValue(chatMessage)
    }
}
